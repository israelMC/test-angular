import { Component, OnInit } from '@angular/core';
import { Routes, Router } from '@angular/router';
import { EmpleadosService } from 'src/app/servicio/empleados.service';
import { Persona } from 'src/app/persona/persona.component';


@Component({
  selector: 'app-editar',
  templateUrl: './editar.component.html',
  styleUrls: ['./editar.component.css']
})
export class EditarComponent implements OnInit {

  persona:Persona = new Persona();
  constructor(private router:Router, private Service:EmpleadosService) { }

  ngOnInit(): void {
  }

  Actulai(){
    let id=localStorage.getItem("id");
    this.Service.obtenerId(+id);
  }
}
