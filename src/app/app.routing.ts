import { ListaComponent } from './lista/lista.component';
import { AgregarComponent } from './agregar/agregar.component';
import { Routes } from '@angular/router';

export const appRoutes:Routes=[
    {path: 'lista', component:ListaComponent},
    {path:'agregar', component:AgregarComponent}
];