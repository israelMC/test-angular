import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from '../servicio/empleados.service';

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  agregarEmpleadoRegistro: any = {id:'', data:{nombre:'', apellidoPat:'', 
  apellidoMat:'', email:''}}

  constructor( private empleadosService:EmpleadosService) { }

  ngOnInit(): void {
  }

  agregarEmpleado(){
    console.log("evento agregar")

    this.empleadosService.agregarEmplado(this.agregarEmpleadoRegistro).subscribe(resultado =>{
      
    }, 
    error=>{
      console.log(JSON.stringify(error));
    });

  }

}
