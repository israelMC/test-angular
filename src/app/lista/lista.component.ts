import { Component, OnInit } from '@angular/core';
import { EmpleadosService } from '../servicio/empleados.service';
import { jitOnlyGuardedExpression } from '@angular/compiler/src/render3/util';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {

  
  empleados: any;
  constructor(private empleadosService:EmpleadosService) { 
    this.obtenerEmpleados();
  }

  ngOnInit(): void {
  }

  obtenerEmpleados(){

    this.empleadosService.obtenerTodoEmpleados().subscribe(resultado => {
      this.empleados = resultado;
    },
    error => {
      console.log(JSON.stringify(error));
    });

  }

  eliminarEmpleado(identificador){
    console.log("evento eliminar")

    this.empleadosService.eliminarEmpleado(identificador).subscribe(resultado =>{
       return this.obtenerEmpleados();
    },
    error=>{
      console.log(JSON.stringify(error)); 
    });

  }
  EliminarTodosEmpleados(){
    this.empleadosService.eliminarDatos().subscribe(resultado =>{
     return this.obtenerEmpleados();
    },
    error => {
      console.log(JSON.stringify(error));
    });
  }
}
