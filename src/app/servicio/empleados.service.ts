import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpClientModule} from'@angular/common/http';
import { Observable } from 'rxjs';
import {map, catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  constructor( private httpClient: HttpClient) { }

  obtenerTodoEmpleados(): Observable<any>{
    return this.httpClient.get("http://localhost:8071/readAllData");

  }

  agregarEmplado(empleado: any){
    let json = JSON.stringify(empleado);

    let headers = new HttpHeaders().set('Content-type', 'application/json');

    return this.httpClient.post("http://localhost:8071/createData/", json, {headers:headers});

  }

  eliminarEmpleado(identificador): Observable<any>{
    return this.httpClient.delete("http://localhost:8071/deleteData?id=" + identificador);

  }
  eliminarDatos():Observable<any>{
    return this.httpClient.delete("http://localhost:8071/deleteAllData?");
  }

  obtenerId(id:number){ 
    return this.httpClient.get

  }

  actualizarEmpleado(){

  }
}
