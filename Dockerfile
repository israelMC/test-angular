#Imagen a utilizar
FROM node:alpine as builder

#Repositorio
WORKDIR '/app'

#intalar angular
RUN npm install -g @angular/cli@10.0.0
COPY package.json .

RUN npm install
COPY . .

RUN ng build

FROM nginx
COPY --from=builder /app/dist/Test-Project /usr/share/nginx/html
EXPOSE 80